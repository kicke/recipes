import cherrypy
from datetime import datetime, timedelta, timezone
import hashlib
import json
import jwt
import sqlite3
import urllib.request

from db.user import User
from settings import hunter_secret, jwt_secret


def authenticate(email, password):
    user = User().get_by_email(email)
    if not user:
        return False

    hashed_password = hash_password(password)
    if hashed_password == user['password']:
        token = create_token(user['id'])
        return token

    return False


def check_email(email, first_name, last_name):
    domain = email.split('@')[1]
    url = 'https://api.hunter.io/v2/email-finder?domain={0}&first_name={1}&last_name={2}&api_key={3}'.format(
        domain, first_name, last_name, hunter_secret
    )
    with urllib.request.urlopen(url) as response:
        r = response.read()
        r_str = r.decode('utf-8')
        r_json = json.loads(r_str)
        result = r_json['data']['email']

    if result:
        return True
    return False


def hash_password(password):
    """Create password hash."""
    h = hashlib.new('ripemd160')
    byte_password = password.encode()
    h.update(byte_password)
    hashed_password = h.hexdigest()

    return hashed_password


def create_token(user_id):
    now = datetime.utcnow()
    issued = now
    expires = now + timedelta(hours=1)
    payload = {
        'uid': user_id,
        'iat': issued,
        'exp': expires,
    }
    token = jwt.encode(payload, jwt_secret, algorithm='HS256').decode('utf-8')

    return token


def verify_token(token):
    data = None
    try:
        data = jwt.decode(token, jwt_secret, algorithms=['HS256'])
    except Exception as e:
        cherrypy.log(str(e))

    return data
