import cherrypy
import json

from api.recipes import RecipesAPI
from db.user import User
from db.recipe import Recipe, Ingredient, RecipeIngredient, Rating
from decorators import token
from settings import app_conf, email_lookup
from templates import env
from utils.authentication import authenticate, hash_password, check_email


class Root(object):
    @cherrypy.expose
    def index(self):
        template = env.get_template('index.html')
        return template.render()

    @cherrypy.expose
    def login(self, email, password):
        token = authenticate(email, password)
        if token:
            return json.dumps({'status': 200, 'token': token})
        else:
            raise cherrypy.HTTPError(status=403)


class Users(object):
    @cherrypy.expose
    @token
    @cherrypy.tools.json_out()
    def index(self):
        result = User().get_all()
        return json.dumps({'status': 200, 'data': result})

    @cherrypy.expose
    def register(self):
        template = env.get_template('user_register.html')
        return template.render()

    @cherrypy.expose
    def register_post(self, email, password, first_name, last_name):
        if email_lookup:
            email_confirmed = check_email(email, first_name, last_name)
            if not email_confirmed:
                raise cherrypy.HTTPError(status=403)

        user = dict()
        user['email'] = email
        user['password'] = hash_password(password)
        user['first_name'] = first_name
        user['last_name'] = last_name
        result = User().create(user)

        if not result:
            raise cherrypy.HTTPError(status=500)

        raise cherrypy.HTTPRedirect('/')


class Recipes(object):
    def _cp_dispatch(self, vpath):
        if len(vpath) == 3:
            # /recipes/rate/recipe_id/rate
            if vpath[0] == 'rate':
                cherrypy.request.params['recipe_id'] = vpath.pop()
                return self.rate

        if len(vpath) == 0:
            return self

    @cherrypy.expose
    @token
    def create(self, name, text, ingredient_ids):
        user = cherrypy.session.get('user', None)
        recipe = dict()
        recipe['user_id'] = user['id']
        recipe['name'] = name
        recipe['text'] = text
        try:
            recipe_id = Recipe.create(recipe)[0]
            if recipe_id:
                for ingredient_id in ingredient_ids:
                    RecipeIngredient.create(recipe_id, int(ingredient_id))
            Rating.create(recipe_id)
        except Exception:
            return json.dumps({'status': 500, 'data': 'Error'})

        return json.dumps({'status': 200, 'data': 'Success'})

    @cherrypy.expose
    def index(self):
        result = Recipe.get_all()
        return json.dumps({'status': 200, 'data': result})

    @cherrypy.expose
    def rate(self, recipe_id, rate):
        result = Rating.increase(int(recipe_id), int(rate))

        if not result:
            return json.dumps({'status': 500, 'data': 'Error'})
        return json.dumps({'status': 200, 'data': 'Success'})


class Ingredients(object):
    @cherrypy.expose
    @token
    def create(self, name):
        result = Ingredient.create(name)
        if not result:
            return json.dumps({'status': 500, 'data': 'Error'})
        return json.dumps({'status': 200, 'data': 'Success'})


class API(object):
    recipes = RecipesAPI()


if __name__ == '__main__':
    cherrypy.config.update({
        'log.screen': True,
        'server.socket_host': '127.0.0.1',
        'server.socket_port': 8888
    })

    root = Root()
    root.ingredients = Ingredients()
    root.recipes = Recipes()
    root.users = Users()
    root.api = API()

    cherrypy.quickstart(root, '/', app_conf)
