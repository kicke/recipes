import cherrypy
import os


database = 'data.db'
jwt_secret = '962pcCT6EPOpI_z0vJE-_yS1mC7ZltiUfpiWS0d7PtLXBPfpenhlawRn8kyo'

def get_env_var(key):
    result = None
    try:
        result = os.environ[key]
    except KeyError:
        pass
    return result

hunter_secret = get_env_var('HUNTER_SECRET')
email_lookup = False

app_conf = {
    '/': {
        'tools.sessions.on': True,
        'tools.sessions.secure': False,
        'tools.sessions.httponly': False,
        'tools.secureheaders.on': False,
        'tools.staticdir.root': os.path.abspath(os.getcwd()),
        'log.access_file': './access.log',
        'log.error_file': './error.log'
    },
    '/api': {
        'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
        'tools.sessions.on': True,
        'tools.response.headers.on': True,
        'tools.repsonse_headers.headers': [('Content-Type', 'text/plain')]
    },
    '/static': {
        'tools.staticdir.on': True,
        'tools.staticdir.dir': './static'
    },
}
