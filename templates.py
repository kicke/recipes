from jinja2 import Environment, FileSystemLoader
import os


templates_dir = os.path.join('static', 'html')
env = Environment(loader=FileSystemLoader(templates_dir))
