import cherrypy
import sqlite3
from settings import database


def execute(query, params=tuple(), many=False, empty=False):
    result = 0

    with sqlite3.connect(database) as conn:
        c = conn.cursor()
        try:
            c.execute(query, params)

            if many:
                result = c.fetchall()
            elif empty:
                conn.commit()
                result = 1
            else:
                result = c.fetchone()

        except Exception as e:
            cherrypy.log(str(e))

    return result
