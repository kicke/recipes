from .db import execute


class Recipe:

    @classmethod
    def create(cls, recipe):
        query = """
INSERT INTO recipes (user_id, name, text)
VALUES (?, ?, ?)
"""
        params = (recipe['user_id'], recipe['name'], recipe['text'])
        result = execute(query, params, empty=True)

        query = """
SELECT recipe_id FROM recipes WHERE user_id=? AND name=?
"""
        params = (recipe['user_id'], recipe['name'])
        result = execute(query, params)

        return result

    @classmethod
    def get_all(cls):
        result = None
        query = """
SELECT name, text FROM recipes
"""
        fetch = execute(query, many=True)
        if fetch:
            result = list()
            for row in fetch:
                d = dict()
                d['name'] = row[0]
                d['text'] = row[1]
                result.append(d)

        return result

    @classmethod
    def get(cls, recipe_id):
        query = 'SELECT recipe_id, name, text FROM recipes WHERE recipe_id=?'
        params = (recipe_id,)
        fetch = execute(query, params)
        result = None
        if fetch:
            result = {'id': fetch[0], 'name': fetch[1], 'text': fetch[2]}
        return result

    @classmethod
    def update(cls, recipe_id, name, text):
        query = 'UPDATE recipes SET name=?, text=? WHERE recipe_id=?'
        params = (name, text, recipe_id)
        result = execute(query, params, empty=True)
        return result

    @classmethod
    def delete(cls, recipe_id):
        query = 'DELETE FROM recipes WHERE recipe_id=?'
        params = (recipe_id)
        result = execute(query, params, empty=True)
        return result


class Ingredient:

    @classmethod
    def create(cls, name):
        query = """
INSERT INTO ingredients (name)
VALUES (?)
"""
        params = (name,)
        result = execute(query, params, empty=True)
        return result


class RecipeIngredient:

    @classmethod
    def create(cls, recipe_id, ingredient_id):
        query = """
INSERT INTO recipe_ingredient (recipe_id, ingredient_id)
VALUES (?, ?)
"""
        params = (recipe_id, ingredient_id)
        result = execute(query, params, empty=True)
        return result

    @classmethod
    def delete(cls, recipe_id):
        query = 'DELETE FROM recipe_ingredient WHERE recipe_id=?'
        params = (recipe_id,)
        result = execute(query, params, empty=True)
        return result


class Rating:

    @classmethod
    def create(cls, recipe_id):
        query = """
INSERT INTO ratings (recipe_id) VALUES (?)
"""
        params = (recipe_id,)
        result = execute(query, params, empty=True)
        return result

    @classmethod
    def increase(cls, recipe_id, rate):
        if rate == 1:
            query = """
UPDATE ratings set one_star = one_star + 1
WHERE recipe_id = ?
"""
        elif rate == 2:
            query = """
UPDATE ratings set two_star = two_star + 1
WHERE recipe_id = ?
"""
        if rate == 3:
            query = """
UPDATE ratings set three_star = three_star + 1
WHERE recipe_id = ?
"""
        if rate == 4:
            query = """
UPDATE ratings set four_star = four_star + 1
WHERE recipe_id = ?
"""
        if rate == 5:
            query = """
UPDATE ratings set five_star = five_star + 1
WHERE recipe_id = ?
"""
        params = (recipe_id,)
        result = execute(query, params, empty=True)
        return result
