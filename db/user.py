from .db import execute


class User(object):
    def create(self, user):
        query = """
INSERT INTO users (email, password, first_name, last_name)
VALUES (?, ?, ?, ?)
"""
        params = (user['email'], user['password'], user['first_name'], user['last_name'])
        result = execute(query, params, empty=True)
        return result

    def get_by_email(self, email):
        result = None
        query = """
SELECT user_id, email, password, first_name, last_name FROM users WHERE email=?
"""
        params = (email,)
        fetch = execute(query, params)
        if fetch:
            result = dict()
            result['id'] = fetch[0]
            result['email'] = fetch[1]
            result['password'] = fetch[2]
            result['first_name'] = fetch[3]
            result['last_name'] = fetch[4]

        return result

    def get_all(self):
        result = None
        query = """
SELECT user_id, email, first_name, last_name FROM users
"""
        fetch = execute(query, many=True)

        if fetch:
            result = list()
            for row in fetch:
                d = dict()
                d['user_id'] = row[0]
                d['email'] = row[1]
                d['first_name'] = row[2]
                d['last_name'] = row[3]
                result.append(d)

        return result
