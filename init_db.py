import sqlite3
from settings import database


def initialize_database():
    with sqlite3.connect(database) as conn:
        print('creating db tables...')
        c = conn.cursor()
        c.execute('PRAGMA foreign_keys=ON')
        c.execute(
            ''' 
            CREATE TABLE IF NOT EXISTS users
            (
            user_id INTEGER PRIMARY KEY AUTOINCREMENT,
            email TEXT UNIQUE NOT NULL,
            password TEXT NOT NULL,
            first_name TEXT NOT NULL,
            last_name TEXT NOT NULL
            )
            '''
        )
        c.execute(
            '''
            CREATE TABLE IF NOT EXISTS recipes
            (
            recipe_id INTEGER PRIMARY KEY AUTOINCREMENT,
            user_id INTEGER NOT NULL,
            name TEXT NOT NULL,
            text TEXT NOT NULL,
            FOREIGN KEY (user_id)
            REFERENCES users(user_id)
            ON DELETE CASCADE,
            UNIQUE (user_id, name)
            )
            '''
        )
        c.execute(
            '''
            CREATE TABLE IF NOT EXISTS ingredients
            (
            ingredient_id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT UNIQUE NOT NULL
            )
            '''
        )
        c.execute(
            '''
            CREATE TABLE IF NOT EXISTS recipe_ingredient
            (
            recipe_id INTEGER NOT NULL,
            ingredient_id INTEGER NOT NULL,
            FOREIGN KEY (recipe_id)
            REFERENCES recipes(recipe_id)
            ON DELETE CASCADE,
            FOREIGN KEY (ingredient_id)
            REFERENCES ingredients(ingredient_id)
            ON DELETE CASCADE
            )
            '''
        )
        c.execute(
            '''
            CREATE TABLE IF NOT EXISTS ratings
            (
            rating_id INTEGER PRIMARY KEY AUTOINCREMENT,
            recipe_id INTEGER UNIQUE NOT NULL,
            one_star INTEGER DEFAULT 0,
            two_star INTEGER DEFAULT 0,
            three_star INTEGER DEFAULT 0,
            four_star INTEGER DEFAULT 0,
            five_star INTEGER DEFAULT 0,
            FOREIGN KEY (recipe_id)
            REFERENCES recipes(recipe_id)
            ON DELETE CASCADE
            )
            '''
        )



initialize_database()
