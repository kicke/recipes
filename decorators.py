import cherrypy
from utils.authentication import verify_token


def token(func):
    def func_wrapper(*args, **kwargs):
        try:
            header_token = cherrypy.request.headers['Authorization']
        except KeyError:
            raise cherrypy.HTTPError(status=403)

        token_data = verify_token(header_token)
        if token_data:
            user = dict()
            user['id'] = token_data['uid']
            cherrypy.session['user'] = user

            return func(*args, **kwargs)

        raise cherrypy.HTTPError(status=403)

    return func_wrapper
