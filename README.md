An application for users and recipes, providing API with token authentication.

First set up virtualenv with python3 and install requirements.

Initiate database. <br />
$ python init_db.py

Run the application. <br />
$ python app.py

Register a user through a browser, by navigating to http://127.0.0.1:8888/register

Login from command line with your email and password, thus recieving the token (install 'jq' linux package first): <br />
$ TOKEN=$(curl http://127.0.0.1:8888/login -d 'email=your@email.com&password=your_password' | jq -r '.token')

Use protected methods: <br />
List users: <br />
$ curl -H "Accept: application/json" -H "Authorization: $TOKEN" http://127.0.0.1:8888/users/

Add ingredient: <br />
http://127.0.0.1:8888/ingredients/create -d 'name=Flour' <br />
http://127.0.0.1:8888/ingredients/create -d 'name=Salt'

Add recipe: <br />
http://127.0.0.1:8888/recipes/create -d 'name=Bread&text=Just bake it, man!&ingredient_ids=1&ingredient_ids=2'

List recipes: <br />
http://127.0.0.1:8888/recipes/

Rate recipe: <br />
params: recipe_id, rate <br />
http://127.0.0.1:8888/recipes/rate/1/5/

Use REST API: <br />
curl -X GET -H "Authorization: $TOKEN" http://127.0.0.1:8888/api/recipes/ <br />
curl -X POST -H "Authorization: $TOKEN" http://127.0.0.1:8888/api/recipes/ -d 'name=Doughnuts&text=It's complicated&ingredient_ids=1&ingredient_ids=2' <br />
curl -X PUT -H "Authorization: $TOKEN" http://127.0.0.1:8888/api/recipes/1 -d 'name=Doughnuts&text=Make the dough, then fry it carefully.&ingredient_ids=1&ingredient_ids=2' <br />
curl -X DELETE -H "Authorization: $TOKEN" http://127.0.0.1:8888/api/recipes/1
