import cherrypy
import json
from db.recipe import Recipe, Ingredient, RecipeIngredient, Rating
from decorators import token


@cherrypy.expose
class RecipesAPI(object):

    @token
    def GET(self, recipe_id=None):
        if recipe_id:
            data = Recipe.get(recipe_id)
        else:
            data = Recipe.get_all()

        return json.dumps({'status': 200, 'data': data, 'message': 'Success'})

    @token
    def POST(self, name, text, ingredient_ids): 
        user = cherrypy.session.get('user', None)
        recipe = dict()
        recipe['user_id'] = user['id']
        recipe['name'] = name
        recipe['text'] = text
        try:
            recipe_id = Recipe.create(recipe)[0]
            if recipe_id:
                for ingredient_id in ingredient_ids:
                    RecipeIngredient.create(recipe_id, int(ingredient_id))
            Rating.create(recipe_id)
        except Exception:
            return json.dumps({'status': 500, 'message': 'Error'})

        return json.dumps({'status': 200, 'message': 'Success'})

    @token
    def PUT(self, recipe_id, name, text, ingredient_ids):
        result = Recipe.update(recipe_id, name, text)
        if ingredient_ids:
            # delete existing
            RecipeIngredient.delete(recipe_id)

            # create anew
            for ingredient_id in ingredient_ids:
                RecipeIngredient.create(recipe_id, int(ingredient_id))

        if not result:
            return json.dumps({'status': 500, 'message': 'Error'})

        return json.dumps({'status': 200, 'message': 'Success'})

    @token
    def DELETE(self, recipe_id):
        result = Recipe.delete(recipe_id)
        if not result:
            return json.dumps({'status': 500, 'message': 'Error'})

        return json.dumps({'status': 200, 'message': 'Success'})
